## TITLE for the User Story/Issue to Resolve

> Detail description of the issue to be developed.

### Usage

- How this will be used:
  - Description of how to be used
  - If is going to be executed as a Function/Method
  - If it is going to be use/activated by the end user (click on a button)
  - If it is a response by a web page request (http://localhost/user/register -> would open the web page to register)

### Acceptance criteria (PO)

1. Describe how this WORK/PRODUCT is going to be accepted .

## BDD:

- Scenario 1: Describe the scenario:
- **Given:** You start the description AS THE TARGET USER that would use this
- **Then:** What is the CONSEQUENCE/RESPONSE of the action of the USER
  **AND:** Extra details to be considered.

## TASKS (example)

> At least these task splitting have to be included:

- [ ] Create for view
- [ ] Create the controller
- [ ] Create the Service (if apply)
- [ ] Create unit test
- [ ] Create automated or/and functional tests
- [ ] Code review ev 1
- [ ] Code review Dev 2
- [ ] Profiling (web app only)
